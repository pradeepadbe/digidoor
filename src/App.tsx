import React, { useEffect } from 'react';
import { Button, TextField, Select, FormControl, MenuItem, InputLabel, TextareaAutosize } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import './App.css';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  textAriaWidth:{
    width:"340px"
  },
  colorsGreen:{
    color:"green"
  },
  table: {
    maxWidth: "90%"
  },
  tableBgColor:{
    backgroundColor: "#F5F5F5",
    padding:"5% 8% 5% 8%"
  }
}));

type submittedData = {
  id: number;
  house_id: string;
  owner_name: string;
  gender: string;
  noofpeopleinhouse: string;
  panchayath: string;
  address: string;
  state:string;
};

function App() {
  const classes = useStyles();
  const [name, updateName] = React.useState('');
  const [gender, updateGender] = React.useState('');
  const [noOfPplInhouse, updatenoOfPplInhouse] = React.useState('');
  const [panchayath, updatePanchayath] = React.useState('');
  const [address, updateAddress] = React.useState('');
  const [state, updateState] = React.useState('');
  const [houseData, updateHouseData] = React.useState<submittedData[]>([]);
  const [count, setCount] = React.useState(0);
  const [formSubmitRes, updateFormSubmitRes]= React.useState<any>();

  const useEffectCallback = (): void | (() => void) => {
      fetch('http://test.excellentguideonlinetuition.com/api/house')
      .then(results => results.json())
      .then(data => {
        updateHouseData(data?.detials)
        setCount(1);
      });
    };
    useEffect(useEffectCallback, [count]);

  const handleSubmit = () => {
    const url = "http://test.excellentguideonlinetuition.com/api/house";
    const submittedData = {
      "owner_name": name,
      "gender":gender,
      "noofpeopleinhouse":noOfPplInhouse,
      "panchayath":panchayath,
      "address":address,
      "state":state,
      "house_id":"Test Home 1",
    }

    fetch(url,{
      method:'POST',
      headers: {
          "Content-type": "application/json",
          "Client-Service":"frontend-client",
          "Authorization":"dc5708e686bc7a166bb235f36f064d43",
          "User-ID":"116960",
          "Auth-key":"simplerestapi"
      },
      body:JSON.stringify(submittedData)
  }).then((result)=>{
      result.json().then((res)=>{
          console.warn('res',res)
      })
  }).catch((err) => {
    console.log(err)
    alert(err)
   
});
  }

  const handleName = (event: { target: { value: React.SetStateAction<string>; }; }) => {
    updateName(event.target.value);
  }
  const handleGender = (event) => {
    updateGender(event.target.value as string);
  }
  const handleNoOfPplInhouse = (event: { target: { value: React.SetStateAction<string>; }; })=>{
    updatenoOfPplInhouse(event.target.value)
  }
  const handleState = (event)=>{
    updateState(event.target.value as string)
  }
  const handlePanchayath = (event: { target: { value: React.SetStateAction<string>; }; })=>{
    updatePanchayath(event.target.value)
  }
  const handleAddress = (event: { target: { value: React.SetStateAction<string>; }; })=>{
    updateAddress(event.target.value)
  }

  return (
    <div className="">
        <div className="App">
          <form onSubmit={handleSubmit}>
            <div>
          <FormControl className={classes.formControl}>
            <TextField id="standard-basic" label=" Owner Name"  type="text" value={name} onChange={handleName} required = {true}  />
          </FormControl>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">Gender</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={gender} onClick={handleGender}
            >
              <MenuItem value={'male'}>Male</MenuItem>
              <MenuItem value={'female'}>Female</MenuItem>
            </Select>
          </FormControl>
          </div>
          <div>
          <FormControl className={classes.formControl}>
            <TextField id="standard-basic" label="No.Of People in house"  type="number"  value={noOfPplInhouse} onChange={handleNoOfPplInhouse} />
          </FormControl>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">state</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              onClick={handleState}
              value={state}
            >
              <MenuItem value="ka">Karnataka</MenuItem>
              <MenuItem value="ap">Andhra Pradesh</MenuItem>
            </Select>
          </FormControl>
        </div>
        <div>
            <FormControl className={classes.formControl}>
              <TextField id="standard-basic" label="Panchayath"  type="text"   value={panchayath} onChange={handlePanchayath} />
            </FormControl>     
          </div>
          <div>   
            <FormControl className={classes.formControl}>
              <TextareaAutosize className={classes.textAriaWidth} aria-label="Address" minRows={3} placeholder="Address"  onChange={handleAddress} value={address} />
            </FormControl>
          </div>
          <Button type="submit" variant="contained" color="primary">
            Submit
          </Button>
        </form>
      </div>
      <div className={classes.tableBgColor}>
        <hr/>
          <h2 className={`${'App'} ${classes.colorsGreen}`} color="green">Submitted Data</h2>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>House id</TableCell>
                  <TableCell >Owner name</TableCell>
                  <TableCell >Gender</TableCell>
                  <TableCell >No.Of People in house</TableCell>
                  <TableCell >State</TableCell>
                  <TableCell >Panchayath</TableCell>
                  <TableCell >Address</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {houseData.map((row) => (
                  <TableRow key={row.id}>                    
                    <TableCell>{row.house_id}</TableCell>
                    <TableCell >{row.owner_name}</TableCell>
                    <TableCell >{row.gender}</TableCell>
                    <TableCell >{row.noofpeopleinhouse}</TableCell>
                    <TableCell >{row.state}</TableCell>
                    <TableCell >{row.panchayath}</TableCell>
                    <TableCell >{row.address}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          
      </div>

    </div>
  );
}

export default App;
